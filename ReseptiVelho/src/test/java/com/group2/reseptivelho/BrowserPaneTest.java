/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.group2.reseptivelho;

import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Cell;
import javafx.scene.control.ListView;
import javafx.scene.control.Slider;
import javafx.scene.layout.VBox;
import org.testfx.framework.junit.ApplicationTest;
import javafx.stage.Stage;
import org.junit.Assert;
import org.junit.Test;
import static org.testfx.api.FxAssert.verifyThat;
import static org.testfx.matcher.control.ListViewMatchers.hasItems;
import org.testfx.service.query.NodeQuery;



/**
 * GUI tests for BrowserScene
 * @author Ryhmä 2. Topi Eronen, Ville Kautonen, Robert Karijärvi
 */
public class BrowserPaneTest extends ApplicationTest{
    Stage stage;
    /**
     * TestFX initialization method
     * @param stage ,JavaFX stage used in finding gui elements
     */
    @Override 
    public void start(Stage stage) {
        ReseptiVelho.init();
        ReseptiVelhoGUI gui = new ReseptiVelhoGUI();
        Scene scene = gui.getBrowserScene();
        this.stage = stage;
        this.stage.setScene(scene);
        this.stage.show();
    }
    
    /**
     * Tests ingredient adding to the ingredientlist. Successful when add is
     * successful.
     */
    @Test
    public void testIngredientAdd() {
        // when:
        NodeQuery nq = lookup("#ingredientlist");
        ListView lv = nq.queryListView();
        int lvsize = lv.getItems().size();
        clickOn("#ingredienttyper").write("testapple");
        clickOn("#addingredient");
        // then:
        nq = from(lv).lookup("testapple");
        Cell node = nq.query();
        verifyThat(lv, hasItems(lvsize+1));
    }
    
    /**
     * Tests ingredient rating. Suceesful when newly created item is rated 2.
     */
    @Test
    public void testIngredientRate() {
        // when:
        NodeQuery nq = lookup("#ingredientlist");
        ListView lv = nq.queryListView();
        nq = from(lv).lookup("testapple");
        Node node = nq.query();
        clickOn(node);
        nq = lookup("#ingredientrater");
        Slider rater = nq.query();
        rater.toString();
        for (Node subNode : rater.getChildrenUnmodifiable()) {
            if (subNode.getStyleClass().get(0)=="thumb") {
                node = subNode;
            }
        }
        drag(node);
        dropBy(50, 0);
        // then:
        Assert.assertTrue(rater.getValue()==2.0);
    }
    
    /**
     * Tests ingredient searching. Succesful when query returns nothing.
     */
    @Test
    public void testIngredientSearch() {
        // when:
        NodeQuery nq = lookup("#ingredientlist");
        ListView lv = nq.queryListView();
        nq = from(lv).lookup("testapple");//.nth(lv.getItems().size()-1);
        Node node = nq.query();
        clickOn(node);
        clickOn("#search");
        // then:
        nq = lookup("#vbox");
        VBox vbox = nq.query();
        Assert.assertTrue(vbox.getChildren().isEmpty());
    }
    
    /**
     * Tests ingredient removal. 
     * Succesful when ingredient is removed from the list
     */
    @Test
    public void testIngredientRemoval() {
        // when:
        NodeQuery nq = lookup("#ingredientlist");
        ListView lv = nq.queryListView();
        nq = from(lv).lookup("testapple");//.nth(lv.getItems().size()-1);
        Node node = nq.query();
        clickOn(node);
        clickOn("#deleteingredient");
        // then:
        Assert.assertFalse(lv.getItems().contains("testapple"));
    }
    
    
}
