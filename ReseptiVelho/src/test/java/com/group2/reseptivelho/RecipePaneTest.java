/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.group2.reseptivelho;

import javafx.scene.Scene;
import javafx.stage.Stage;
import org.junit.Assert;
import org.junit.Test;
import org.testfx.framework.junit.ApplicationTest;
import org.testfx.service.query.NodeQuery;

/**
 * GUI tests for the RecipePane claas
 * @author Ryhmä 2. Topi Eronen, Ville Kautonen, Robert Karijärvi
 */
public class RecipePaneTest extends ApplicationTest{
    Stage stage;
    /**
     * TestFX initialization method
     * @param stage ,JavaFX stage used in finding gui elements
     */
    @Override
    public void start(Stage stage) {
        ReseptiVelho.init();
        ReseptiVelhoGUI gui = new ReseptiVelhoGUI();
        Scene scene = new Scene(new RecipePane(1, gui), 1024, 768);
        this.stage = stage;
        this.stage.setScene(scene);
        this.stage.show();
    }
    
    /**
     * Verifies that back button exists
     * 
     */
    @Test
    public void testBackButton() {
        //expect:
        NodeQuery nq = lookup("#back");
        Assert.assertNotNull(nq.query());

    }
    
}
