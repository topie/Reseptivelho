package com.group2.reseptivelho;

import com.group2.reseptivelho.api.Food2ForkAPI;
import com.group2.reseptivelho.data.RecipeList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class APIFood2ForkTest {

    private Food2ForkAPI api;
    
    public APIFood2ForkTest() {
        api = new Food2ForkAPI();
    }

    @BeforeClass
    public static void setUpClass() {
        assertNull(ReseptiVelho.init());
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testSearch() {
        RecipeList result = api.searchRecipes("bacon");
        assertFalse(result.isEmpty());
    }

    @Test
    public void testInvalidSearch() {
        RecipeList result = api.searchRecipes(
                "baked kryptonite",
                "uranium-233",
                "ground toenails with syrup");
        assertTrue(result.isEmpty());
    }

    @Test
    public void testGetRecipe() {
        assertNotNull(api.getRecipeByID("35120"));
    }

    @Test
    public void testInvalidGetRecipe() {
        assertNull(api.getRecipeByID("surely a nonexisting id"));
    }
}
