package com.group2.reseptivelho;

import com.group2.reseptivelho.api.EdamamAPI;
import com.group2.reseptivelho.data.RecipeList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class APIEdamamTest {

    private EdamamAPI api;

    public APIEdamamTest() {
        api = new EdamamAPI();
    }

    @BeforeClass
    public static void setUpClass() {
        assertNull(ReseptiVelho.init());
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testSearch() {
        if (true) {
            return;
        }
        RecipeList result = api.searchRecipes("bacon");
        assertFalse(result.isEmpty());
    }

    @Test
    public void testInvalidSearch() {
        RecipeList result = api.searchRecipes(
                "baked kryptonite",
                "uranium-233",
                "ground toenails with syrup");
        assertTrue(result.isEmpty());
    }
}
