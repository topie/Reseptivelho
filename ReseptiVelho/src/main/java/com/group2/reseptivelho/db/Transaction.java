package com.group2.reseptivelho.db;

/**
 * Support for SQL transactions. Use Reseptivelho.db.transaction.begin() to
 * begin a transaction, then Reseptivelho.db.transaction.commit() to commit
 * changes OR Reseptivelho.db.transaction.rollBack() to cancel all changes made
 * to the database during this transaction. Several transactions can be nested,
 * and must each be committed. The last commit is the only one that actually
 * commits the changes.
 *
 * @author Topi Eronen <topi.eronen@metropolia.fi>
 */
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Transaction {

    private int nesting_level;
    private java.sql.Connection connection;

    public Transaction(java.sql.Connection connection) {
        this.connection = connection;
        this.nesting_level = 0;
    }

    public void begin() {
        if (this.nesting_level < 0) {
            this.nesting_level = 0;
        }
        if (++this.nesting_level == 1) {
            try {
                this.connection.setAutoCommit(false);
            } catch (SQLException e) {
                Logger.getLogger(Transaction.class.getName()).log(Level.SEVERE,
                        null, e);
            }
        }
    }

    public void commit() {
        if (--this.nesting_level == 0) {
            try {
                this.connection.commit();
                this.connection.setAutoCommit(true);
            } catch (SQLException e) {
                Logger.getLogger(Transaction.class.getName()).log(Level.SEVERE,
                        null, e);
            }
        }
        if (this.nesting_level < 0) {
            this.nesting_level = 0;
        }
    }

    public void rollBack() {
        if (this.nesting_level > 0) {
            try {
                this.connection.rollback();
                this.connection.setAutoCommit(true);
            } catch (SQLException e) {
                Logger.getLogger(Transaction.class.getName()).log(Level.SEVERE,
                        null, e);
            }
        }
        this.nesting_level = 0;
    }
}
