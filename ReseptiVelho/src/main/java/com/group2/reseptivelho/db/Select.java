package com.group2.reseptivelho.db;

import java.util.ArrayList;
import java.util.Arrays;
import static com.group2.reseptivelho.db.JoinMethod.*;

public class Select extends Query<Select> {

    // Is DISTINCT to be included?
    private boolean distinct = false;

    // Columns to be included in SELECT (uses '*' if empty)
    private final ArrayList<String> columns;

    // Tables to be joined in (raw SQL)
    private final ArrayList<String> joins;

    // Separator between WHERE clauses
    private final String operator = "AND";

    // Result set ordering (raw SQL to be inserted right after "ORDER BY ")
    private String order = null;

    // Row count limitations (uses "LIMIT max OFFSET min" when both set)
    private int min = -1, max = -1;

    public Select(Connection db, String table) {
        super(db, table);
        this.columns = new ArrayList<>();
        this.joins = new ArrayList<>();
    }

    public static Select from(Connection db, String table) {
        return new Select(db, table);
    }

    public Select join(String table, String... conditions) {
        return this.join(LEFT, table, conditions);
    }

    public Select join(JoinMethod method, String table, String... conditions) {

        StringBuilder clause = new StringBuilder();

        clause.append(method.toString());

        // Remove duplicate whitespaces
        table = table.replaceAll("(\\s)\\1", "$1");
        table = table.replace(" AS ", "` AS `");
        clause.append(" `").append(table).append('`');

        boolean first = true;
        if (conditions.length > 0) {
            for (String c : conditions) {
                if (first) {
                    clause.append(" ON (");
                    first = false;
                } else {
                    clause.append(") AND (");
                }
                clause.append(c);
            }
            clause.append(')');
        }

        this.joins.add(clause.toString());

        return this;
    }

    public Select columns(String... columns) {
        if (columns.length == 0) {
            this.columns.clear();
        } else {
            this.columns.addAll(Arrays.asList(columns));
        }
        return this;
    }

    /**
     * Turns this select into a SELECT DISTINCT instead of a plain SELECT.
     *
     * @return this
     */
    public Select distinct() {
        return this.distinct(true);
    }

    /**
     * Turns the DISTINCT part of SELECT DISTINCT either on or off for this
     * particular select.
     *
     * @param enable
     * @return this
     */
    public Select distinct(boolean enable) {
        this.distinct = enable;
        return this;
    }

    /**
     * Adds or removes result set ordering.
     *
     * @param order SQL order statement or null to disable ordering
     * @return
     */
    public Select order(String order) {
        this.order = order;
        return this;
    }

    /**
     * Crops the result set by specifying both the first row (offset) and the
     * maximum number of rows returned.
     *
     * @param offset max starting index (rows 0 ... offset will be omitted)
     * @param count max number of returned rows
     * @return this
     */
    public Select limit(int offset, int count) {
        this.min = offset;
        this.max = count;
        return this;
    }

    /**
     * Limits the result set size.
     *
     * @param count max number of returned rows
     * @return this
     */
    public Select limit(int count) {
        this.min = -1;
        this.max = count;
        return this;
    }

    @Override
    /**
     * Combines this select into a single Clause object
     *
     * @return Clause SQL statement that is ready to be executed
     */
    public Clause toClause() {
        StringBuilder prefix = new StringBuilder();
        StringBuilder suffix = new StringBuilder();
        prefix.append("SELECT ");

        // DISTINCT
        if (this.distinct) {
            prefix.append("DISTINCT ");
        }

        // Form columns
        if (this.columns.isEmpty()) {
            prefix.append('*');
        } else {
            boolean first = true;
            for (String s : this.columns) {
                if (first) {
                    first = false;
                } else {
                    prefix.append(", ");
                }

                if (s.indexOf(' ') == -1 && s.indexOf('*') == -1) {
                    // Add local table name if none specified to avoid
                    // integrity constraint violations
                    prefix.append('`').append(this.table).append("`.`")
                            .append(s).append("` AS `").append(s).append('`');
                } else {
                    // Add SQL clauses as they were
                    prefix.append(s);
                }
            }
        }

        // Add table name
        prefix.append(" FROM `").append(this.table).append('`');

        // Join tables
        for (String join : this.joins) {
            prefix.append(' ').append(join);
        }

        if (this.parameters.size() > 0) {
            // Join parameters
            prefix.append(" WHERE (");

            // (parameter list gets added here)
            // Terminate parentheses
            suffix.append(')');
        }

        // Order result set
        if (this.order != null) {
            suffix.append(" ORDER BY ").append(this.order);
        }

        // Limit row count
        if (this.max > -1) {
            suffix.append(" LIMIT ").append(this.max);
            if (this.min > -1) {
                suffix.append(" OFFSET ").append(this.min);
            }
        }

        // Terminate statement
        suffix.append(';');

        return Clause.from(
                prefix.toString(),
                suffix.toString(),
                ") " + this.operator + " (",
                this.parameters.toArray(new Clause[this.parameters.size()]));
    }
}
