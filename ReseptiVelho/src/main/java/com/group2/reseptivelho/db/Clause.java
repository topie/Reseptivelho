package com.group2.reseptivelho.db;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Locale;

public class Clause {

    protected String body;
    protected ArrayList<Object> parameters;

    public Clause(String body, Object... parameters) {
        this.body = body;

        // Add the correct amount of parameters
        // (fills in NULLS if there's not enough)
        this.parameters = new ArrayList<>();
        for (int i = 0, pos = 0; (pos = body.indexOf('?', pos) + 1) != 0;) {
            if (parameters != null && i < parameters.length) {
                this.parameters.add(parameters[i++]);
            } else {
                this.parameters.add(null);
            }
        }
    }

    /**
     * Returns a Clause's formatting string, which should be a part of an SQL
     * statement with values replaced as question marks ('?'). For example
     * "WHERE id = ?" and "VALUES (?, ?, ?)".
     *
     * @return body text as a formatted string
     */
    public String getBody() {
        return this.body;
    }

    /**
     * Returns stored parameters in the order of their appearance. For example
     * for a Clause constructed as new Clause("WHERE id=? AND name=?", 1,
     * "foo"), this would return an ArrayList containing: { Integer(1),
     * String("foo") }.
     *
     * @return ArrayList object containing all parameters stored in the Clause
     */
    public ArrayList<Object> getParameters() {
        return this.parameters;
    }

    /**
     * Concatenates a bunch of clauses together.
     *
     * @param prefix optional (unformatted) SQL to prepend the clauses
     * @param suffix optional (unformatted) SQL to trail the clauses
     * @param separator optional string to be inserted in-between each clause
     * @param clauses array of clauses to join together
     * @return one clause to rule them all, ready to be prepared and executed
     */
    public static Clause from(String prefix, String suffix, String separator,
            Clause... clauses) {
        StringBuilder body = new StringBuilder();
        body.append(prefix);
        ArrayList<Object> parameters = new ArrayList<>();
        boolean first = true;
        for (Clause c : clauses) {
            if (first) {
                first = false;
            } else {
                body.append(separator);
            }
            body.append(c.body);
            parameters.addAll(c.parameters);
        }
        body.append(suffix);

        return new Clause(body.toString(), parameters.toArray());
    }

    /**
     * Crude string representation for debugging only. Use prepared statements
     * instead, because this is not safe.
     *
     * @return SQL query
     */
    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        for (int i = 0, current = -1, prev;;) {
            prev = current + 1;
            current = this.body.indexOf('?', prev);

            if (current == -1 || i >= this.parameters.size()) {
                s.append(this.body.substring(prev));
                break;
            }

            s.append(this.body.substring(prev, current));

            Object parameter = this.parameters.get(i++);

            if (parameter == null) {
                s.append("NULL");
            } else if (parameter instanceof Number) {
                if (((Number) parameter).intValue()
                        == ((Number) parameter).doubleValue()) {
                    // Integer
                    s.append(parameter.toString());
                } else {
                    // Decimal
                    s.append(String.format(Locale.ROOT, "%f",
                            (Number) parameter));
                }
            } else {
                // String
                s.append('\'')
                        .append(parameter.toString().replace("'", "\\'"))
                        .append('\'');
            }
        }

        return s.toString();
    }
}
