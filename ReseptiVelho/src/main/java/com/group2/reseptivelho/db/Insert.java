/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.group2.reseptivelho.db;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class for INSERT-queries. Note: This class has been designed so that it
 * inserts values into columns in a table of your choosing and as such may or
 * may not work beyond it's intended capabilities for it has not been tested in
 * other circumstances (for now). So exercise caution when using in a not
 * intended purpose. (Used in adding ingredients to the ingredients list)
 *
 * Example query: INSERT INTO 'yourtable' (yourcolumn1, yourcolumn2...) VALUES
 * (?, ?...);
 *
 * @author Ville
 */
public class Insert {

    //db connection
    protected Connection db;

    // Clause parameters
    protected ArrayList<Clause> parameters;

    // Table to be inserted into
    protected String table;

    // Columns to be inserted into (inserts to every column if left empty)
    private final ArrayList<String> columns;

    // Injection safe statement ready for execution
    protected PreparedStatement statement = null;

    // Used with the Clause.from method, but not really used. 
    // Unless making a statement with operators.
    private final String operator = "";

    /**
     * Constructor
     *
     * @param db, Database connection class used in queries
     * @param table, Name of the table which is used in the insert
     */
    public Insert(Connection db, String table) {
        this.db = db;
        this.table = table;
        this.columns = new ArrayList<>();
        this.parameters = new ArrayList<>();
    }

    /**
     * Used to initialize query
     *
     * @param db, Database connection class used in queries
     * @param table, Name of the table which is used in the insert
     * @return Insert, returns a brand new query ready for use, well almost
     * ready
     */
    public static Insert into(Connection db, String table) {
        return new Insert(db, table);
    }

    /**
     * Defines the parameters in the query
     *
     * @param condition, a String used in the parameter slot/place. usually
     * needed amount of question marks Example: ?, ?, ?
     * @param values, the inserted values that will take the place of ?
     * @return Insert object
     */
    public Insert values(String condition, Object... values) {
        this.parameters.add(new Clause(condition, values));
        return this;
    }

    public Insert columns(String... columns) {
        if (columns.length == 0) {
            this.columns.clear();
        } else {
            this.columns.addAll(Arrays.asList(columns));
        }
        return this;
    }

    /**
     * Forms the INSERT into a Clause object
     *
     * @return Clause, which can then be executed as a prepared statement
     */
    public Clause toClause() {
        StringBuilder prefix = new StringBuilder();
        StringBuilder suffix = new StringBuilder();

        // INSERT-query
        prefix.append("INSERT INTO ");

        // Add table name
        prefix.append(" `").append(this.table).append('`');
        // Form columns
        if (!this.columns.isEmpty()) {
            boolean first = true;
            for (String s : this.columns) {
                if (first) {
                    prefix.append(" (");
                    first = false;
                } else {
                    prefix.append(", ");
                }
                prefix.append(s);
            }
            prefix.append(")");
        }
        prefix.append(" VALUES (");
        // parameter list
        suffix.append(')');

        // Terminate statement
        suffix.append(';');

        return Clause.from(
                prefix.toString(),
                suffix.toString(),
                ") " + this.operator + " (",
                this.parameters.toArray(new Clause[this.parameters.size()]));
    }

    /**
     * Used to prepare a safe statement
     *
     * @return PreparedStatement, the statement ready to be executed
     */
    public PreparedStatement prepare() {
        if (this.statement == null) {
            this.statement = this.db.prepare(this.toClause());
        }
        return this.statement;
    }

    /**
     * Executes the statement (also prepares it)
     *
     * @return int, inserted primary key or 0 on failure
     */
    public int execute() {
        PreparedStatement sql = this.prepare();
        try {
            sql.executeUpdate();

            // Return the last inserted primary key
            ResultSet result = sql.getGeneratedKeys();
            if (result.next()) {
                return result.getInt(1);
            }
        } catch (SQLException e) {
            Logger.getLogger(Query.class.getName()).log(Level.SEVERE,
                    "Failed to execute SQL query", e);
        }

        return 0;
    }
}
