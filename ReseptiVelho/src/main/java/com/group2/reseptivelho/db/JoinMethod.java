package com.group2.reseptivelho.db;

/**
 * Enumeration representing supported SQL JOIN methods. JoinMethod.toString
 * evaluates into a valid SQL representation of each method.
 *
 * @author topie
 */
public enum JoinMethod {
    LEFT {
        @Override
        public String toString() {
            return "LEFT JOIN";
        }
    },
    RIGHT {
        @Override
        public String toString() {
            return "RIGHT JOIN";
        }
    },
    INNER {
        @Override
        public String toString() {
            return "INNER JOIN";
        }
    },
    FULL_OUTER_JOIN {
        @Override
        public String toString() {
            return "FULL OUTER JOIN";
        }
    };
}
