package com.group2.reseptivelho.db;

import java.sql.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * MySQL interface using prepared statements.
 *
 * @author topie
 *
 * NetBeans setup: - Projects - Libraries - Add library - MySQL JDBC Driver -
 * Services - Databases - Drivers - New Driver - Connector/J Driver
 */
public class Connection {

    private java.sql.Connection connection;
    private String username, database, url;
    private boolean connected = false;
    public Transaction transaction;

    public Connection(String url, String username, String password,
            String database) throws ClassNotFoundException, SQLException {
        this.url = url;
        this.username = username;
        this.database = database;

        Class.forName("com.mysql.jdbc.Driver");

        url = "jdbc:mysql://" + url;
        this.connection = DriverManager.getConnection(url, username, password);
        this.connection.createStatement().executeQuery("USE " + database);
        this.connected = true;
        
        this.transaction = new Transaction(this.connection);
    }

    /**
     * Closes the opened connection. Must be called before the program quits.
     *
     * @throws java.sql.SQLException if the connection cannot be closed
     */
    public void close() throws SQLException {
        if (this.connected) {
            this.connection.close();
            this.connected = false;
        }
    }

    @Override
    protected void finalize() throws Throwable {
        try {
            this.close();
        } finally {
            super.finalize();
        }
    }

    public boolean isAlive() {
        return connected;
    }

    /**
     * Returns a prepared SQL statement formed form this clause.
     *
     * @param clause Contains the SQL base and optional parameters
     * @return PreparedStatement
     */
    public PreparedStatement prepare(Clause clause) {
        PreparedStatement statement = null;

        try {
            statement = this.connection.prepareStatement(clause.getBody(),
                    PreparedStatement.RETURN_GENERATED_KEYS);

            ArrayList<Object> parameters = clause.getParameters();

            int i = 1;
            for (Object p : parameters) {

                if (p instanceof Boolean) {
                    statement.setBoolean(i, (boolean) p);
                } else if (p instanceof Byte) {
                    statement.setByte(i, (byte) p);
                } else if (p instanceof Short) {
                    statement.setShort(i, (short) p);
                } else if (p instanceof Integer) {
                    statement.setInt(i, (int) p);
                } else if (p instanceof Long) {
                    statement.setLong(i, (long) p);
                } else if (p instanceof Float) {
                    statement.setFloat(i, (float) p);
                } else if (p instanceof Double) {
                    statement.setDouble(i, (double) p);
                } else if (p instanceof String) {
                    statement.setString(i, (String) p);
                } else {
                    statement.setNull(i, java.sql.Types.NULL);
                }
                i++;
            }

        } catch (SQLException e) {
            Logger.getLogger(Connection.class.getName())
                    .log(Level.SEVERE, null, e);
        }

        return statement;
    }

    @Override
    public String toString() {
        return this.database + " (" + this.username + "@" + this.url + ")";
    }
}
