/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.group2.reseptivelho.db;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class for DELETE-queries. Note: This class has been designed for usage in the
 * deletion button and should be used with caution in other cases
 * 
 * @author Ville
 */
public class Delete {
    //db connection
    protected Connection db;
    
    // Clause parameters
    protected ArrayList<Clause> parameters;
    
    // Table to be deleted from
    protected String table;
    
//    // Columns to be inserted into (inserts to every column if left empty)
//    private final ArrayList<String> columns;
    
    // Injection safe statement ready for execution
    protected PreparedStatement statement = null;
    
    // Used with the Clause.from method, but not really used. 
    // Unless making a statement with operators.
    private final String operator = "";
    
    /**
     * Constructor
     * @param db, Database connection class used in queries
     * @param table, Name of the table which is used in the insert 
     */
    public Delete(Connection db, String table) {
        this.db = db;
        this.table = table;
        this.parameters = new ArrayList<>();
    }
    
    /**
     * Used to initialize query
     * @param db, Database connection class used in queries
     * @param table, Name of the table which is used in the insert
     * @return Insert, returns a brand new query ready for use, well almost ready
     */
    public static Delete from(Connection db, String table) {
        return new Delete(db, table);
    }
    
    /**
     * Defines the parameters in the query
     * @param condition, a String used in the parameter slot/place. usually needed amount of question marks Example: ?, ?, ?
     * @param values, the inserted values that will take the place of ?
     * @return Delete object
     */
    public Delete where(String condition, Object... values) {
        this.parameters.add(new Clause(condition, values));
        return this;
    }
    
    public Delete columns(String... columns) {
    //    if (columns.length == 0) {
    //        this.columns.clear();
    //    } else {
    //        this.columns.addAll(Arrays.asList(columns));
    //    }
        return this;
    }
    
    /**
     * Forms the DELETE into a Clause object
     * @return Clause, which can then be executed as a prepared statement
     */
    public Clause toClause() {
        StringBuilder prefix = new StringBuilder();
        StringBuilder suffix = new StringBuilder();
        
        // DELETE-query
        prefix.append("DELETE FROM ");  

        // Add table name
        prefix.append(" `").append(this.table).append('`');
        // Form columns
//        if (!this.columns.isEmpty()) {
//            boolean first = true;
//            for (String s : this.columns) {
//                if (first) {
//                    prefix.append(" (");
//                    first = false;
//                } else {
//                    prefix.append(", ");
//                }
//                    prefix.append(s);
//            }
//            prefix.append(")");
//        }
        prefix.append(" WHERE (");
        // parameter list
        suffix.append(')');

        // Terminate statement
        suffix.append(';');

        return Clause.from(
                prefix.toString(),
                suffix.toString(),
                ") " + this.operator + " (",
                this.parameters.toArray(new Clause[this.parameters.size()]));
    }
    
    /**
     * Used to prepare a safe statement
     * @return PreparedStatement, the statement ready to be executed
     */
    public PreparedStatement prepare() {
        if (this.statement == null) {
            this.statement = this.db.prepare(this.toClause());
        }
        return this.statement;
    }
    
    /**
     * Executes the statement (also prepares it)
     * @return boolean, confirmation of the success or failure of the operation
     */
    public boolean execute() {
            try {
                this.prepare().executeUpdate();
            } catch (SQLException e) {
                Logger.getLogger(Query.class.getName()).log(Level.SEVERE,
                        "Failed to execute SQL query", e);
                return false;
            }
        
        return true;
    }
}
