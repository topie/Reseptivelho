package com.group2.reseptivelho.db;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Base class for various SQL queries.
 *
 * Provides WHERE functionality and getter functions for polling the query
 * result set. Uses prepared statements which means that it is perfectly safe
 * (and required) to give un-escaped parameters in the where clause.
 *
 * @param <T> is the inherited subclass
 */
public abstract class Query<T> {

    // Query set properties
    protected Connection db;
    protected String table;
    protected ArrayList<Clause> parameters;

    // Result set properties (for when the query has been executed)
    protected PreparedStatement statement = null;
    protected ResultSet results = null;
    protected int cols = 0, rows = 0; // Result set size
    protected ArrayList<String> columnNames = null; // Column names

    /**
     * Query type: (non-updates are expected to return sets of rows and columns,
     * updates only manipulate data and return nothing but meta information at
     * most)
     */
    protected boolean isUpdate = false;

    public Query(Connection db, String table) {
        this.db = db;
        this.table = table;
        this.parameters = new ArrayList<>();
    }

    /**
     * Adds one clause to the where conditions of this query.
     *
     * @param condition SQL clause which may optionally contain question marks
     * ('?') as placeholder characters for the actual values.
     *
     * @param values Zero or more Objects that are to replace the placeholder
     * characters in the condition String. These must be in the same order as
     * the placeholder characters appear. NULLs will fill in for omitted values
     * (too many question marks), and extra values will simply be ignored (not
     * enough question marks).
     *
     * @return this
     */
    public T where(String condition, Object... values) {
        this.parameters.add(new Clause(condition, values));
        return (T) this;
    }

    /**
     * Adds a prebuilt clause object to the where conditions of this query.
     * Useful when wanting to construct subqueries or complex conditionals
     * elsewhere (like nested OR and AND structures).
     *
     * @param clause A Clause object
     *
     * @return this
     */
    public T where(Clause clause) {
        this.parameters.add(clause);
        return (T) this;
    }

    /**
     * Returns all column names from the result set. Executes the query if not
     * done yet.
     *
     * @return list of Strings
     */
    public ArrayList<String> getColumns() {
        this.execute();
        return this.columnNames;
    }

    /**
     * Returns a single column name from the result set. Executes the query if
     * not done yet.
     *
     * @param index Column index, starting from 0.
     * @return Column name in String.
     */
    public String getColumn(int index) {
        this.execute();
        return this.columnNames.get(index);
    }

    /**
     * Returns the full result set or null if the query failed. Executes the
     * query if not done yet.
     *
     * @return List of result rows, each row being a map of Objects containing
     * the returned column values for that row. Map keys are String names for
     * corresponding columns.
     */
    public ArrayList<Map<String, Object>> getAll() {
        ArrayList<Map<String, Object>> out = new ArrayList<>();

        if (this.execute()) {
            try {
                while (this.results.next()) {
                    out.add(this._getResultRow());
                }

                return out;

            } catch (SQLException e) {
                Logger.getLogger(Query.class.getName()).log(Level.SEVERE,
                        "Failed to retrieve all rows from the result set", e);
            }
        }

        return null;
    }

    /**
     * Returns next row from the query of null on no more rows available.
     * Executes the query if not done yet.
     *
     * @return Map of Objects containing the returned column values. Map keys
     * are String names for corresponding columns. Returns null if the query
     * failed or if the requested row number is greater than the row count in
     * the result set.
     */
    public Map<String, Object> getRow() {
        if (this.execute()) {
            try {
                if (this.results.next()) {
                    return this._getResultRow();
                }
            } catch (SQLException e) {
                Logger.getLogger(Query.class.getName()).log(Level.SEVERE,
                        "Failed to retrieve next row from the result set", e);
            }
        }
        return null;
    }

    /**
     * Returns a specific row from the query. Executes the query if not done
     * yet.
     *
     * @param row Row number beginning from 1. Negative numbers are relative to
     * the end of the set (-1 meaning the last row).
     *
     * @return Map of Objects containing the returned column values. Map keys
     * are String names for corresponding columns. Returns null if the query
     * failed or if the requested row number is greater than the row count in
     * the result set.
     */
    public Map<String, Object> getRow(int row) {
        if (this.execute()) {
            try {
                if (this.results.absolute(row)) {
                    return this._getResultRow();
                }

            } catch (SQLException e) {
                Logger.getLogger(Query.class.getName()).log(Level.SEVERE,
                        "Failed to retrieve row #" + row
                        + " from the result set", e);
            }
        }

        return null;
    }

    /**
     * Rewinds the result set so that next getRow() call will return the first
     * row.
     */
    public void rewind() {
        try {
            this.results.first();
        } catch (SQLException e) {
        }
    }

    /**
     * Local helper function: composes the current result set row into a map
     */
    private Map<String, Object> _getResultRow() throws SQLException {
        Map<String, Object> out = new HashMap<>();
        for (int col = 1; col <= this.cols; ++col) {
            out.put(this.columnNames.get(col - 1), this.results.getObject(col));
        }

        return out;
    }

    /**
     * @return Builds a Clause which is a ready-to-be-prepared SQL statement
     */
    abstract public Clause toClause();

    /**
     * @return Builds a prepared statement generated from this query
     */
    public PreparedStatement prepare() {
        if (this.statement == null) {
            this.statement = this.db.prepare(this.toClause());
        }
        return this.statement;
    }

    /**
     * Prepares and executes the query if not done yet. Does nothing if this has
     * been called already, so this can carelessly be called several times
     * without it causing redundant traffic and load.
     *
     * @return true if the query was successfully run, false if it failed.
     */
    public boolean execute() {
        if (this.results == null) {
            try {
                this.columnNames = new ArrayList<>();
                this.cols = 0;

                PreparedStatement s = this.prepare();
                if (this.isUpdate) {
                    s.executeUpdate();
                    return true;
                } else {
                    this.results = s.executeQuery();
                }

                ResultSetMetaData meta = this.results.getMetaData();
                this.cols = meta.getColumnCount();

                this.columnNames.ensureCapacity(this.cols);
                for (int col = 1; col <= this.cols; ++col) {
                    this.columnNames.add(meta.getColumnName(col));
                }

            } catch (SQLException e) {
                this.results = null;
                Logger.getLogger(Query.class.getName()).log(Level.SEVERE,
                        "Failed to execute SQL query", e);
                return false;
            }
        }
        return true;
    }

    @Override
    public String toString() {
        return this.toClause().toString();
    }
}
