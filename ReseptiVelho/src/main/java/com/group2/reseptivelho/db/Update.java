package com.group2.reseptivelho.db;

import java.util.ArrayList;

/**
 * Class for UPDATE-queries.
 *
 * @author Topi Eronen (topi.eronen@metropolia.fi)
 */
public class Update extends Query<Update> {

    // Updated values (filter conditions are stored in super.parameters)
    protected ArrayList<Clause> values;

    // Separator between WHERE clauses
    private final String operator = "AND";

    /**
     * Constructor
     *
     * @param db, Database connection class used in queries
     * @param table, Name of the destination table
     */
    public Update(Connection db, String table) {
        super(db, table);
        this.values = new ArrayList<>();
        this.isUpdate = true;
    }

    /**
     * Constructs a new update query.
     *
     * @param db, Database connection
     * @param table, Name of the table in which the update will be performed.
     * @return Update
     */
    public static Update table(Connection db, String table) {
        return new Update(db, table);
    }

    public Update set(String key, Object value) {
        this.values.add(new Clause(key + " = ?", value));
        return this;
    }

    /**
     * Forms the INSERT into a Clause object
     *
     * @return Clause, which can then be executed as a prepared statement or
     * null if no parameters or no conditions have been specified
     */
    @Override
    public Clause toClause() {
        return (this.values.size() * this.parameters.size() > 0)
                ? Clause.from("UPDATE `" + this.table + '`', ";", "",
                        Clause.from(" SET ", "", ", ",
                                this.values.toArray(
                                        new Clause[this.values.size()])),
                        Clause.from(" WHERE ", "", this.operator,
                                this.parameters.toArray(
                                        new Clause[this.parameters.size()])))
                : null;
    }
}
