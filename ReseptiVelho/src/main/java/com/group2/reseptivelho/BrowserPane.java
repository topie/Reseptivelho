/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.group2.reseptivelho;

import com.group2.reseptivelho.data.Ingredient;
import com.group2.reseptivelho.data.Recipe;
import com.group2.reseptivelho.data.RecipeList;
import com.group2.reseptivelho.db.Delete;
import com.group2.reseptivelho.db.Insert;
import com.group2.reseptivelho.db.Select;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

/**
 * Root Node Class for the browserscene
 * @author Ryhmä 2. Ville Kautonen, Topi Eronen, Robert Karijärvi
 */
public class BrowserPane extends BorderPane{
    // GUI widgets
    private ListView ingredientlist;
    private TextField ingredienttyper;
    private RecipeList recentsearch;
    private VBox vbox;
    private final static Image rating_icon[] = new Image[2];
    private ImageView[] rating_indicator;
    private Slider ingredientrater;
    private Scene recipescene;
    private CheckBox strictSearch;
    protected final ReseptiVelhoGUI gui_instance;
    
    /**
     * Constructor
     * @param gui, reference to the GUI
     */
    public BrowserPane(ReseptiVelhoGUI gui) {
        super();
        this.gui_instance = gui;
        
        BorderPane root = new BorderPane();
        root.setPadding(new Insets(10, 10, 20, 10));

        rating_icon[0] = new Image("/img/rate-0.png");
        rating_icon[1] = new Image("/img/rate-1.png");

        //Recipe list creation
        vbox = new VBox();
        vbox.setId("vbox");
        ScrollPane scrollrecipes = new ScrollPane();
        scrollrecipes.setPrefViewportWidth(512);
        scrollrecipes.setPrefViewportHeight(690);
        vbox.setPadding(new Insets(10));
        vbox.setSpacing(8);

        Text title = new Text(ReseptiVelho.messages.getString("browsertitle"));
        title.setId("titles");
        StackPane titlebg = new StackPane();
        titlebg.getChildren().add(title);
        titlebg.setId("image");

        scrollrecipes.setContent(vbox);
        BorderPane recipesntitle = new BorderPane();
        
        // Right-hand pane for all ingredient-related stuff
        BorderPane ingredientpane = new BorderPane();

        // Selected ingredient details
        final GridPane ingredientdetails = new GridPane();
        ingredientdetails.setHgap(5);
        ingredientdetails.setVgap(10);
        ingredientdetails.setPadding(new Insets(20, 0, 0, 75));
        ingredientdetails.setVisible(false);
        final Label ingredientname = new Label();
        ingredientname.setId("image");
        ingredientdetails.add(ingredientname, 0, 0, 5, 1);
        
        this.rating_indicator = new ImageView[5];
        for (int i = 0; i < 5; ++i) {
            this.rating_indicator[i] = new ImageView(rating_icon[0]);
            this.rating_indicator[i].setPreserveRatio(true);
            this.rating_indicator[i].setFitHeight(24);
            ingredientdetails.add(this.rating_indicator[i], i, 1, 1, 1);
        }

        this.ingredientrater = new Slider();
        this.ingredientrater.setId("ingredientrater");
        this.ingredientrater.setMin(0);
        this.ingredientrater.setMax(5);
        this.ingredientrater.setShowTickLabels(true);
        this.ingredientrater.setShowTickMarks(true);
        this.ingredientrater.setMajorTickUnit(1);
        this.ingredientrater.setMinorTickCount(0);
        this.ingredientrater.setBlockIncrement(1);
        this.ingredientrater.valueProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue v0, Object o0, Object o1) {
                Ingredient ingredient = getSelectedIngredient();
                if (updateIngredientRating() && ingredient != null)
                {
                    ingredient.save();
                }
            }
        });

        //Ingredient adder
        BorderPane ingredients = new BorderPane();
        ingredients.setPadding(new Insets(30, 80, 0, 0));
        ingredienttyper = new TextField();
        ingredienttyper.setPrefWidth(250);
        ingredienttyper.setId("ingredienttyper");
        Button addingredient = new Button();
        addingredient.setId("addingredient");
        addingredient.setText(ReseptiVelho.messages.getString("ingredientadd"));
        addingredient.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (!ingredienttyper.getText().equals("")) {
                    Select check = Select.from(ReseptiVelho.db, "ingredients")
                            .columns("name")
                            .where("name LIKE ?", ingredienttyper.getText());
                    if (check.getRow() == null) {
                        Insert adder = Insert.into(ReseptiVelho.db, "ingredients")
                                .columns("name")
                                .values("?", ingredienttyper.getText());
                        adder.execute();
                        refreshList();
                        System.out.println(ReseptiVelho.messages.getString("debugingredientadded"));
                    } else {
                        System.out.println(ReseptiVelho.messages.getString("debugingredientalreadyindb"));
                    }
                }
            }

        });

        Text addingredienttitle = new Text(ReseptiVelho.messages.getString("ingredientaddtitle"));
        StackPane addingredienttitlebg = new StackPane();
        addingredienttitlebg.getChildren().add(addingredienttitle);
        addingredienttitlebg.setId("image");
        
        

        //List of the ingredients
        ingredientlist = new ListView();
        ingredientlist.setId("ingredientlist");
        ingredientlist.setPrefSize(300, 400);
        ingredientlist.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        ingredientlist.getSelectionModel().getSelectedItems().addListener(
                new ListChangeListener() {
            @Override
            @SuppressWarnings("empty-statement")
            public void onChanged(ListChangeListener.Change c) {
                Ingredient ingredient = getSelectedIngredient();

                // Display details only iff one ingredient if selected
                if (ingredient != null) {
                    ingredientname.setText(ingredient.name.toUpperCase());
                    ingredientrater.setValue(ingredient.rating);
                    ingredientdetails.setVisible(true);
                } else {
                    // Otherwise hide the property/rating area thingy
                    ingredientdetails.setVisible(false);
                }
            }
        });

        refreshList();
        //Delete button
        Button deleteingredient = new Button();
        deleteingredient.setId("deleteingredient");
        deleteingredient.setText(ReseptiVelho.messages.getString("ingredientdelete"));
        deleteingredient.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Ingredient deletee = getSelectedIngredient();
                if (deletee != null) {
                    System.out.println(ReseptiVelho.messages.getString("debugingredientdelete"));
                    Delete deleter = Delete.from(ReseptiVelho.db, "ingredients")
                            .where("id = ?", deletee.id);
                    deleter.execute();
                    refreshList();
                }
            }
        });
        
        //Search checkbox (restrict to selected ingredients only)
        strictSearch = new CheckBox(ReseptiVelho.messages.getString("searchstrict"));
        strictSearch.setId("strictSearch");
        strictSearch.setSelected(false);

        //Search button
        Button search = new Button();
        search.setId("search");
        search.setText(ReseptiVelho.messages.getString("searchbrowse"));
        search.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (!ingredientlist.getSelectionModel().isEmpty()) {
                    List<Ingredient> searchees = ingredientlist
                            .getSelectionModel().getSelectedItems();
                    String searcheesarray[] = new String[searchees.size()];
                    int i = 0;
                    for (Ingredient searchee : searchees) {
                        searcheesarray[i++] = searchee.toString();
                    }
                    System.out.println(ReseptiVelho.messages.getString("debugsearch") + Arrays.toString(searcheesarray));
                    recentsearch = Recipe.getByKeywords(searcheesarray);
                    if (strictSearch.isSelected()) {
                        recentsearch.removeBut(searcheesarray);
                    }
                    refreshRecipes(recentsearch, vbox);
                }
            }
        });
        
        //Assembling and formatting nodes together
        //lvl 5
        BorderPane ingredientlistntitle = new BorderPane();
        Text ingredientlisttitle = new Text(ReseptiVelho.messages.getString("ingredientlist"));
        StackPane ingredientlisttitlebg = new StackPane();
        ingredientlisttitlebg.getChildren().add(ingredientlisttitle);
        ingredientlisttitlebg.setId("image");
        ingredientlistntitle.setTop(ingredientlisttitlebg);
        ingredientlistntitle.setBottom(ingredientlist);
        //lvl 4
        BorderPane bottomofingredients = new BorderPane();
        bottomofingredients.setPadding(new Insets(0, 0, 30, 0));
        BorderPane bottomRow = new BorderPane();
        bottomRow.setLeft(deleteingredient);
        bottomRow.setRight(search);
        bottomRow.setBottom(strictSearch);
        bottomofingredients.setTop(ingredientlistntitle);
        bottomofingredients.setBottom(bottomRow);
        
        //lvl 3
        ingredientdetails.add(this.ingredientrater, 0, 2, 5, 1);
        ingredients.setTop(addingredienttitlebg);
        ingredients.setLeft(ingredienttyper);
        ingredients.setRight(addingredient);
        ingredients.setBottom(bottomofingredients);
        //lvl 2
        ingredientpane.setTop(ingredientdetails);
        ingredientpane.setBottom(ingredients);
        recipesntitle.setTop(titlebg);
        recipesntitle.setCenter(scrollrecipes);
        recipesntitle.setPadding(new Insets(0,0,30,0));
        //lvl 1
        root.setLeft(recipesntitle);
        root.setRight(ingredientpane);
        //lvl 0
        this.setCenter(root);
        
    }
    
        /**
     * Used to show the recipelist in the GUI based on the search
     *
     * @param recipelist, an arraylist containing the recipes to be shown
     */
    private void refreshRecipes(RecipeList recipelist, VBox vbox) {
        vbox.getChildren().clear();
        for (Recipe recipe : recipelist) {
            Button btn = new Button();
            final int recipeindex = recipe.id;
            btn.setText(recipe.name
                    + " (" + recipe.rating
                    + "/5)");
            btn.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    System.out.println(ReseptiVelho.messages.getString("debugscreenchange"));
                    recipescene = new Scene(new RecipePane(recipeindex, gui_instance), 1024, 768);
                    recipescene.getStylesheets().add("reseptivelho.css");
                    gui_instance.stage.setScene(recipescene);
                }

            });

            vbox.getChildren().add(btn);
        }
    }

    /**
     * Used to get the ingredient list from the db (Usually after add and
     * deletion operations) and showing them in the ingredientlist.
     *
     */
    private void refreshList() {
        Select ingredients;
        ingredients = Select.from(ReseptiVelho.db, "ingredients")
                .columns("id", "name", "rating")
                .order("rating DESC, name ASC");
        Map<String, Object> row;
        ObservableList<Ingredient> listitems = FXCollections.observableArrayList();
        while ((row = ingredients.getRow()) != null) {
            Ingredient i = new Ingredient();
            i.name = (String) row.get("name");
            i.id = (int) (long) row.get("id");
            i.rating = (int) row.get("rating");
            listitems.add(i);
        }
        ingredientlist.setItems(listitems);
    }
    
    /**
     * Counts selected ingredient items
     * @return Ingredient
     */
    private Ingredient getSelectedIngredient() {
        // Count number of selected items manually, because providing such a method in JavaFX would apparently be too generic and
        // prevent us from creating Möbius ListViews using the same interface and its implementation family.
        // This is how you get Nokia. People can always write more code to stay employed, but it does not make the code useful.
        // Seriously. Fuck java mentality.
        Ingredient ingredient = null;
        int java_on_paskaa = 0;
        for (Iterator i = this.ingredientlist.getSelectionModel()
                .getSelectedItems().iterator(); i.hasNext();) {
            ingredient = (Ingredient) i.next();
            java_on_paskaa++;
        }

        return (java_on_paskaa == 1)
                ? ingredient
                : null;
    }
    
    /**
     * Updates ingredient ratings
     * @return true if successful otherwise false
     */
    private boolean updateIngredientRating() {

        int new_rating = (int) Math.round(this.ingredientrater.getValue());
        this.ingredientrater.setValue(new_rating);
        
        for (int i = 0; i < 5; ++i) {
            int n = (this.ingredientrater.getValue() > i) ? 1 : 0;
            this.rating_indicator[i].setImage(
                    rating_icon[n]);
        }

        Ingredient ingredient = this.getSelectedIngredient();
        if (ingredient != null && ingredient.rating != new_rating)
        {
            ingredient.rating = new_rating;
            return true;
        }
        
        return false;
    }
}

