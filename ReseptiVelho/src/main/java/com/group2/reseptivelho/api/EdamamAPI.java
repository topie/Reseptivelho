package com.group2.reseptivelho.api;

import com.group2.reseptivelho.data.Recipe;
import com.group2.reseptivelho.data.RecipeIngredient;
import com.group2.reseptivelho.data.RecipeList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class EdamamAPI extends API {

    public EdamamAPI() {
        super("edamam");
    }

    /**
     * Search recipes from the Edamam API
     *
     * @param keyword list of ingredients to match
     * @return RecipeList object containing search results
     */
    @Override
    public RecipeList getRecipesByKeywords(String... keyword) {
        Map<String, String> query = new HashMap<>();
        query.put("q", keyword[0]); // Edamam only supports 1 ingredient
        RecipeList recipes = new RecipeList();

        JSONObject result = this.getREST("search", query);
        if (result == null) {
            return recipes;
        }

        Iterator<JSONObject> hits = ((JSONArray) result.get("hits")).iterator();
        while (hits.hasNext()) {
            JSONObject hit = hits.next();
            hit = (JSONObject) hit.get("recipe");

            Recipe recipe = new Recipe();
            // Basic recipe data
            recipe.name = (String) hit.get("label");
            recipe.url = (String) hit.get("url");
            recipe.image = (String) hit.get("image");
            recipe.external_id = (String) hit.get("shareAs");

            // Ingredients
            Iterator<JSONObject> ingredients;
            ingredients = ((JSONArray) hit.get("ingredients")).iterator();

            /**
             * Edamam's ingredients are inconsistent and thus difficult to
             * parse. Use another API where applicable.
             */
            while (ingredients.hasNext()) {
                JSONObject item = ingredients.next();
                RecipeIngredient ingredient = new RecipeIngredient();
                ingredient.text = (String) item.get("text");
                recipe.ingredients.add(ingredient);
            }

            if (!recipe.hasAllergens()) {
                recipes.add(recipe);
            }
        }

        recipes.sort();
        return recipes;
    }

    @Override
    public Recipe getRecipeByID(String id) {
        throw new UnsupportedOperationException("Not implemented yet.");
    }
}
