package com.group2.reseptivelho.api;

/**
 * Base REST API class that is to be extended with an API-specific class.
 * Provides functions getREST and buildURL for calling any REST API.
 */

/*
 USAGE EXAMPLE:
 System.out.println(ReseptiVelho.api.getRecipeByID("35120"));

 Iterator<Recipe> i = ReseptiVelho.api.searchRecipes("chicken").iterator();
 while (i.hasNext()) { System.out.println(i.next()); }
 */
import com.group2.reseptivelho.ReseptiVelho;
import com.group2.reseptivelho.data.Recipe;
import com.group2.reseptivelho.data.RecipeList;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.*;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public abstract class API {

    protected String api_url;
    protected ArrayList<String> keys;
    protected final String charset = "UTF-8";

    public API(String apiName) {
        apiName = "api." + ((apiName.equals("")) ? "" : apiName + '.');
        this.api_url = ReseptiVelho.get(apiName + "url");

        // Read authentication tokens (API keys) from config
        this.keys = new ArrayList<>();
        for (int i = 0;; ++i) {
            String key = apiName + "key[" + i + ']';
            if ((key = ReseptiVelho.get(key)) == null) {
                break;
            }

            this.keys.add(key);
        }
    }

    /**
     * Returns an array of recipes whose ingredients match at least some of the
     * given keywords. Calls derived getRecipesByKeywords internally.
     *
     * @param keyword list of keywords to match (some APIs only support one, in
     * which case the others are silently discarded)
     * @return RecipeList containing the search results as Recipe objects
     */
    @SuppressWarnings("empty-statement")
    final public RecipeList searchRecipes(String... keyword) {
        RecipeList results = new RecipeList();

        for (; // Upside-down smirk because this is just plain dirty:
                // Get recipes and loop until the result set is not empty
                keyword.length > 0 &&
                (results = this.getRecipesByKeywords(keyword)).isEmpty();
                // If no results were found, drop last ingredient and try again
                keyword = Arrays.copyOf(keyword, keyword.length - 1));

        return results;
    }

    /**
     * Returns an array of recipes whose ingredients match given keywords.
     *
     * @param keyword list of keywords to match (some APIs only support one, in
     * which case the others are silently discarded)
     * @return list of Recipe objects
     */
    protected abstract RecipeList getRecipesByKeywords(String... keyword);

    /**
     * Returns recipe details from the API, or null if not found.
     *
     * @param id that identifies the recipe in the API (external id)
     * @return single Recipe object
     */
    public abstract Recipe getRecipeByID(String id);

    /**
     * Performs a GET call to the REST API and returns results as a JSON object.
     *
     * @param method REST method to call
     * @param parameters map of parameters as key-value pairs
     * @return JSONObject of the results or null on failure.
     */
    protected JSONObject getREST(String method, Map<String, String> parameters) {
        try {
            HttpURLConnection connection;
            connection = (HttpURLConnection) new URL(
                    this.buildURL(method, parameters))
                    .openConnection();

            connection.setRequestMethod("GET");
            connection.setRequestProperty("Accept", "text/html,application/json");
            connection.setRequestProperty("Accept-Charset", charset);

            // Food2Fork flips out and vomits a 403 if User-Agent is missing
            connection.setRequestProperty("User-Agent",
                    "Suck my user agent (RFC says SHOULD, not MUST). Geez.");

            InputStream response_stream = connection.getInputStream();

            Scanner scanner = new Scanner(response_stream);
            String response = scanner.useDelimiter("\\A").next();

            // System.out.println(response);

            JSONParser parser = new JSONParser();
            return (JSONObject) parser.parse(response);
        } catch (Exception e) {
            Logger.getLogger(API.class.getName()).log(Level.SEVERE,
                    "API call failed", e);
        }

        return null;
    }

    /**
     * Concatenates API URL and given parameters together. Adds authentication
     * keys to the URL as well.
     *
     * @param method REST method name
     * @param parameters map of parameters as key-value pairs
     * @return URL with given GET parameters
     */
    protected String buildURL(String method, Map<String, String> parameters) {
        StringBuilder url = new StringBuilder();
        url.append(this.api_url).append('/').append(method);

        // Add all authentication parameters
        char delimiter = '?';
        Iterator<String> i = this.keys.iterator();
        while (i.hasNext()) {
            url.append(delimiter).append(i.next());
            delimiter = '&';
        }

        // Add actual call parameters
        try {
            for (Map.Entry<String, String> e : parameters.entrySet()) {
                url.append('&')
                        .append(URLEncoder.encode(e.getKey(), charset))
                        .append('=')
                        .append(URLEncoder.encode(e.getValue(), charset));
            }
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(
                    charset + " ain't supported. Go buy a new computer.", e);
        }

        return url.toString();
    }
}
