package com.group2.reseptivelho.api;

import com.group2.reseptivelho.data.Recipe;
import com.group2.reseptivelho.data.RecipeIngredient;
import com.group2.reseptivelho.data.RecipeList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class Food2ForkAPI extends API {

    public Food2ForkAPI() {
        super("food2fork");
    }

    /**
     * Search recipes from the Food2Fork API
     *
     * @param keyword list of ingredients to match
     * @return RecipeList object containing search results
     */
    @Override
    public RecipeList getRecipesByKeywords(String... keyword) {
        Map<String, String> query = new HashMap<>();
        query.put("q", String.join(",", keyword));
        RecipeList recipes = new RecipeList();

        JSONObject result = this.getREST("search", query);
        if (result == null) {
            return recipes;
        }

        Iterator<JSONObject> hits;
        hits = ((JSONArray) result.get("recipes")).iterator();
        while (hits.hasNext()) {
            String recipe_id = (String) hits.next().get("recipe_id");
            Recipe recipe = this.getRecipeByID(recipe_id);

            if (!(recipe == null || recipe.hasAllergens())) {
                recipes.add(recipe);
            }
        }

        recipes.sort();
        return recipes;
    }

    /**
     * Get a single recipe from the Food2Fork  API
     *
     * @param id Internal API id uniquely identifying the recipe
     * @return Recipe object or null if not found
     */
    @Override
    public Recipe getRecipeByID(String id) {
        
        Recipe recipe = Recipe.getByExternalId(id);
        if (recipe != null) {
            return recipe;
        }
        
        Map<String, String> query = new HashMap<>();
        query.put("rId", id);

        JSONObject result = this.getREST("get", query);

        if (result.isEmpty()) {
            return null;
        }
        
        try {
            recipe = new Recipe();
            result = (JSONObject) result.get("recipe");

            recipe.external_id = (String) result.get("recipe_id");
            recipe.name = (String) result.get("title");
            recipe.url = (String) result.get("source_url");
            recipe.image = (String) result.get("image_url");

            Iterator<String> ingredients;
            ingredients = ((JSONArray) result.get("ingredients")).iterator();

            while (ingredients.hasNext()) {
                String item = ingredients.next();
                RecipeIngredient ingredient = new RecipeIngredient();

                ingredient.text = item.replace("\n", "").replace("\r", "");
                recipe.ingredients.add(ingredient);
            }
        } catch (ClassCastException e) {
            return null;
        }
        
        recipe.save();

        return recipe;
    }
}
