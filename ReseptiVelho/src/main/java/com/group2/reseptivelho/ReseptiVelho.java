package com.group2.reseptivelho;

import com.group2.reseptivelho.api.*;
import com.group2.reseptivelho.db.*;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;
import static javafx.application.Application.launch;


public class ReseptiVelho {

    // Database connection
    public static Connection db;
    
    public static API api;

    // General config data container (use get method to access)
    protected static Properties settings = null;
    
    // Current localization (Default only as a failsafe)
    protected static ResourceBundle messages =ResourceBundle.getBundle("MessagesBundle", new Locale("en", "GB"));
    
    /**
     * Performs all necessary initialization tasks
     *
     * @return null on success, otherwise a short description of what went wrong
     */
    public static String init() {

        ReseptiVelho.api = new Food2ForkAPI();
        
        try {
            // Connect to the db
            ReseptiVelho.db = new Connection(
                    ReseptiVelho.get("db.hostname"),
                    ReseptiVelho.get("db.username"),
                    ReseptiVelho.get("db.password"),
                    ReseptiVelho.get("db.database"));

        } catch (ClassNotFoundException ex) {
            return "JDBC Driver not correctly installed";

        } catch (SQLException ex) {
            return "Failed to connect to the database";
        }

        Locale currentLocale = new Locale(
                ReseptiVelho.get("gui.language"), 
                ReseptiVelho.get("gui.country"));
        
        messages = ResourceBundle.getBundle("MessagesBundle", currentLocale);
        
        return null;
    }

    /**
     * Main method for the lulz.
     *
     * (!) NOTE: JavaFX doesn't always run this, depending on how the project is
     * built. DO NOT RELY ON THIS METHOD ever being run.
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    /**
     * Convenience function for accessing preferences stored in "settings.cfg",
     * which it reads at the first call.
     *
     * @author topie
     * @param setting property name
     * @return property value
     */
    public static String get(String setting) {
        // Load settings at first call
        if (settings == null) {
            settings = new Properties();
            try (FileInputStream file = new FileInputStream("settings.cfg")) {
                settings.load(file);
            } catch (IOException e) {
                System.out.println("(!) Failed to load configuration file: "
                        + e.getMessage());
            }
        }

        return settings.getProperty(setting);
    }
}
