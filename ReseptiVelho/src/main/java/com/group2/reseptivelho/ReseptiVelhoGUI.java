package com.group2.reseptivelho;

/**
 *
 * @author Ryhmä 2. Topi Eronen, Ville Kautonen, Robert Karijärvi
 *
 * GUI. Create and initialize the JavaFX graphical user interface
 */
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;

public class ReseptiVelhoGUI extends Application {

    protected Scene browserscene;
    protected Stage stage;

    /**
     * The actual main method of the program
     *
     * @param primaryStage , which is the default stage used in JavaFX
     */
    @Override
    public void start(Stage primaryStage) {
        // Wake up the slumbering wizard
        String error = ReseptiVelho.init();
        if (error == null) {
            // Creates and sets the starting scene
            stage = primaryStage;
            browserscene = new Scene(new BrowserPane(this), 1024, 768);
            browserscene.getStylesheets().add("reseptivelho.css");
            primaryStage.setTitle(ReseptiVelho.messages.getString("maintitle"));
            primaryStage.setScene(browserscene);
            primaryStage.show();
        } else {
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle(ReseptiVelho.messages.getString("maintitle"));
            alert.setHeaderText("Startup failed");
            alert.setContentText(error);

            alert.showAndWait();
        }
    }
    
    /**
     * Getter. Used by the TestFx tests
     * @return Scene return the browserscene
     */
    public Scene getBrowserScene() {
        if (browserscene==null) {
            browserscene = new Scene(new BrowserPane(this), 1024, 768);
        }
        return browserscene;
    }
}
