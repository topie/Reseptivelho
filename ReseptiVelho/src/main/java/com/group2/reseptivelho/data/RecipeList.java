package com.group2.reseptivelho.data;

import java.util.ArrayList;

import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Spliterator;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Container object for zero or more Recipe objects. Works quite like a vanilla
 * Java List, but is fixed to accept Recipes only and offers additional methods
 * like sorting elements and removing recipes by keyword-matching.
 *
 * @author topie
 */
public class RecipeList implements Iterable<Recipe> {

    protected ArrayList<Recipe> recipes;

    public RecipeList() {
        this.recipes = new ArrayList<>();
    }

    /**
     * Removes all of the recipes from this list. The list will be empty after
     * this call returns.
     */
    public void clear() {
        this.recipes.clear();
    }

    /**
     * Returns true if this list contains no elements
     *
     * @return true if this list contains no elements
     */
    public boolean isEmpty() {
        return this.recipes.isEmpty();
    }

    /**
     * Adds a recipe object at the last element of this list. If null is given,
     * does nothing.
     *
     * @param recipe object to be added
     */
    public void add(Recipe recipe) {
        if (recipe != null) {
            this.recipes.add(recipe);
        }
    }

    /**
     * Returns a single recipe by index
     *
     * @param index
     * @return removed Recipe object
     */
    public Recipe get(int index) {
        return this.recipes.get(index);
    }

    /**
     * Removes a single recipe by index
     *
     * @param index
     * @return removed Recipe object
     */
    public Recipe remove(int index) {
        Recipe r = this.get(index);
        this.recipes.remove(index);

        return r;
    }

    /**
     * Removes all recipes whose ingredients match given keywords. The lookup is
     * case-insensitive.
     *
     * @param keywords list of search keywords to exclude recipes by
     */
    public void remove(String... keywords) {
        // Lowercase all keywords (This could be done with an efficient
        // one-liner that documents itself, but since we are doing Java, that is
        // obviously out of the question.)
        List<String> lcKeywords = (List<String>) ((List<String>) Arrays
                .asList(keywords))
                .stream()
                .map(new Function() {
                    @Override
                    public Object apply(Object t) {
                        return ((String) t).toLowerCase();
                    }
                }).collect(Collectors.toList());

        for (Iterator<Recipe> recipe = this.recipes.iterator();
                recipe.hasNext();) {

            Recipe r = recipe.next();

            // Look for the first match in all ingredients
            boolean match = false;
            for (RecipeIngredient i : r.ingredients) {
                String ingredient = i.text.toLowerCase();

                for (String s : lcKeywords) {
                    if ((match = ingredient.contains(s))) {
                        break;
                    }
                }

                // Drop a matching recipe and continue to the next one
                if (match) {
                    recipe.remove();
                    break;
                }
            }
        }
    }

    /**
     * Removes all recipes that do not have at least one ingredient matching
     * given keywords. The lookup is case-insensitive.
     *
     * @param keywords list of search keywords to exclude recipes by
     */
    public void removeBut(String[] keywords) {
        // Lowercase all keywords (This could be done with an efficient
        // one-liner that documents itself, but since we are doing Java, that is
        // obviously out of the question.)
        List<String> lcKeywords = (List<String>) ((List<String>) Arrays
                .asList(keywords))
                .stream()
                .map(new Function() {
                    @Override
                    public Object apply(Object t) {
                        return ((String) t).toLowerCase();
                    }
                }).collect(Collectors.toList());

        for (Iterator<Recipe> recipe = this.recipes.iterator();
                recipe.hasNext();) {

            Recipe r = recipe.next();

            // Count ingredients that match keywords
            int matches = 0;
            for (RecipeIngredient i : r.ingredients) {
                String ingredient = i.text.toLowerCase();

                for (String s : lcKeywords) {
                    if (ingredient.contains(s)) {
                        matches++;
                    }
                }
            }

            // Drop recipes that contain extra ingredients
            if (matches < r.ingredients.size()) {
                recipe.remove();
            }
        }
    }

    /**
     * Sorts contained recipes according to the comparison function defined in
     * Recipe class
     */
    public void sort() {
        Collections.sort(this.recipes);
    }


    @Override
    public Iterator<Recipe> iterator() {
        return this.recipes.iterator();
    }

    @Override
    public Spliterator<Recipe> spliterator() {
        return this.recipes.spliterator();
    }

    @Override
    public void forEach(Consumer action) {
        this.recipes.forEach(action);
    }


    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();

        for (Recipe r : this.recipes) {
            s.append("- ").append(r).append("\n");
        }

        return s.toString();
    }

}
