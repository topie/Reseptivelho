package com.group2.reseptivelho.data;

import com.group2.reseptivelho.ReseptiVelho;
import com.group2.reseptivelho.db.Insert;

/**
 * Meal ingredient that is part of a recipe. These are essentially strings
 * returned by a third-party API. Due to inconsistencies, not much can be
 * assumed of the data contained here, but usually it they have a nonstandard
 * textual representation of quantity and quality (e.g. "1 tablespoon of minced
 * dragon eggs"). APIs may also (incorrectly) return placeholders or HTML
 * identities as recipe ingredients, and we will just have to deal with them.
 *
 * @author topie
 */
public class RecipeIngredient {

    /**
     * TODO: change scope to protected and use getters instead
     */
    public int id = 0;
    public int recipe_id = 0;
    public String text;

    public RecipeIngredient() {
    }

    /**
     * Saves this RecipeIngredient object as a row in the db. New row will be
     * generated and object's id updated for new objects. Otherwise id stays
     * unmodified and only the altered db columns are updated.
     *
     * @return true on success, false if failed
     */
    public boolean save() {

        this.id = Insert.into(ReseptiVelho.db, "recipe_ingredients")
                .columns("text", "recipe_id")
                .values("?, ?", this.text, this.recipe_id)
                .execute();

        return this.id != 0;
    }

    @Override
    public String toString() {
        return this.text;
    }
}
