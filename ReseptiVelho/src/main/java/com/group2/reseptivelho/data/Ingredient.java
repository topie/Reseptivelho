package com.group2.reseptivelho.data;

import com.group2.reseptivelho.ReseptiVelho;
import com.group2.reseptivelho.db.Insert;
import com.group2.reseptivelho.db.Update;

/**
 * User-created ingredient. These are the foodstuffs one keeps in their kitchen
 * cabinet. These are used as search keywords when API searches are performed.
 * Ingredients serve another purpose: those rated very low are considered
 * allergens, and search results containing these are not displayed.
 *
 * @author topie
 */
public class Ingredient {

    public static final int NEUTRAL_RATING = 3;

    /**
     * TODO: change scope to protected and use getters instead
     */
    public int id = 0;
    public String name;
    public String external_id = null;
    public int rating;
    public int hidden = 1;

    public Ingredient() {
        this.rating = Ingredient.NEUTRAL_RATING;
    }

    /**
     * Saves this Ingredient object as a row in the db. New row will be
     * generated and object's id updated for new objects. Otherwise id stays
     * unmodified and only the altered db columns are updated.
     *
     * @return true on success, false if failed
     */
    public boolean save() {

        if (this.id > 0) {
            Update.table(ReseptiVelho.db, "ingredients")
                    .set("name", this.name)
                    .set("external_id", this.external_id)
                    .set("rating", this.rating)
                    .set("hidden", this.hidden)
                    .where("id = ?", this.id)
                    .execute();
        } else {
            this.id = Insert.into(ReseptiVelho.db, "recipe_ingredients")
                    .columns("name", "external_id", "rating", "hidden")
                    .values("?, ?", this.name, this.external_id, this.rating, this.hidden)
                    .execute();
        }

        return this.id != 0;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
