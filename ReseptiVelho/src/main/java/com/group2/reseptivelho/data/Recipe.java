package com.group2.reseptivelho.data;

import com.group2.reseptivelho.ReseptiVelho;
import com.group2.reseptivelho.db.Clause;
import com.group2.reseptivelho.db.Delete;
import com.group2.reseptivelho.db.Insert;
import com.group2.reseptivelho.db.Query;
import com.group2.reseptivelho.db.Select;
import com.group2.reseptivelho.db.Update;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

/**
 * A single meal recipe containing 0 or more RecipeIngredients. Recipes are
 * originally obtained from a third-party api and then stored in the db, along
 * with their ingredients.
 *
 * @author topie
 */
public class Recipe implements Comparable {

    /**
     * TODO: change scope to protected and use getters instead
     */
    public String description;
    public String name;
    public String comment;
    public String url;
    public String external_id;
    public String image;
    public int rating = 0;
    public int id = 0;
    public ArrayList<RecipeIngredient> ingredients;

    private double ingredientScore = -1.0d;

    public Recipe() {
        this.ingredients = new ArrayList<>();
    }

    /**
     * Fetches a recipe from the db, identified by its primary key.
     *
     * @param id primary key for the corresponding db row
     * @return Recipe object or null if not found
     */
    public static Recipe getById(int id) {
        return Recipe._getFirstFromQuery(Select.from(ReseptiVelho.db, "recipes")
                .where("id = ?", id));
    }

    /**
     * Fetches a recipe from the db, identified by its API id.
     *
     * @param external_id unique string that identifies the recipe in the API
     * @return Recipe object or null if not found
     */
    public static Recipe getByExternalId(String external_id) {
        return Recipe._getFirstFromQuery(Select.from(ReseptiVelho.db, "recipes")
                .where("external_id = ?", external_id));
    }

    /**
     * Searches recipes that contain given keywords as ingredients. Initially
     * calls the REST API, but if that is not available or returns no matches,
     * results are searched from the local db.
     *
     * @param keywords one or more ingredients
     * @return
     */
    public static RecipeList getByKeywords(String... keywords) {
        if (keywords.length == 0) {
            return new RecipeList();
        }

        // Try the API First
        RecipeList results = ReseptiVelho.api.searchRecipes(keywords);
        if (!results.isEmpty()) {
            return results;
        }

        // In a bout of extreme optimism, search from the local db if API
        // returned no results.
        Clause matchList = null;
        for (int i = 0; i < keywords.length; ++i) {
            // Build a search clause for every ingredient
            // This is inefficient but requires minimal changes to the db API
            Clause match = new Clause(
                    "recipe_ingredients.text LIKE(?)",
                    "%" + keywords[i] + "%");

            // We are desperate, so use OR to match with any keyword
            matchList = (matchList == null)
                    ? match
                    : Clause.from("", "", " OR ", matchList, match);
        }

        results = _getAllFromQuery(
                Select.from(ReseptiVelho.db, "recipes")
                        .distinct()
                        .join("recipe_ingredients",
                                "recipe_ingredients.recipe_id = recipes.id")
                        .columns(
                                "id",
                                "external_id",
                                "name",
                                "description",
                                "comment",
                                "url",
                                "image",
                                "rating")
                        .where(matchList));

        return results;
    }

    /**
     * Executes given select and returns the first result row as a recipe
     * object.
     *
     * @param query A select statement with optional where clauses. This query
     * MUST be created for the 'recipes' table.
     * @return Recipe object or null if the query fails
     */
    private static Recipe _getFirstFromQuery(Select query) {
        return _getFromResultRow(query.getRow());
    }

    /**
     * Executes given select and returns all result rows as an ArrayList of
     * recipe objects.
     *
     * @param query A select statement with optional where clauses. This query
     * MUST be created for the 'recipes' table.
     * @return Recipe object or null if the query fails
     */
    private static RecipeList _getAllFromQuery(Select query) {
        RecipeList results = new RecipeList();

        Map<String, Object> row;
        while ((row = query.getRow()) != null) {
            results.add(_getFromResultRow(row));
        }

        return results;
    }

    /**
     * Builds a Recipe object from a result row.
     *
     * @param row Result row from a query from the recipes table.
     * @return Recipe object (or null for a null row)
     */
    private static Recipe _getFromResultRow(Map<String, Object> row) {
        if (row == null) {
            return null;
        }

        Recipe recipe = new Recipe();
        recipe.id = (int) (long) row.get("id");
        recipe.external_id = (String) row.get("external_id");
        recipe.name = (String) row.get("name");
        recipe.description = (String) row.get("description");
        recipe.comment = (String) row.get("comment");
        recipe.url = (String) row.get("url");
        recipe.image = (String) row.get("image");
        recipe.rating = (int) row.get("rating");

        recipe._getIngredientsFromDB();
        return recipe;

    }

    /**
     * Refreshes ingredient list. Ditches all unsaved ingredients and fetches
     * those stored in the db.
     */
    private void _getIngredientsFromDB() {
        this.ingredients.clear();

        Query query = Select.from(ReseptiVelho.db, "recipe_ingredients")
                .where("recipe_id = ?", this.id);

        Map<String, Object> result;
        while ((result = query.getRow()) != null) {
            RecipeIngredient i = new RecipeIngredient();
            i.id = (int) (long) result.get("id");
            i.recipe_id = (int) (long) result.get("recipe_id");
            i.text = (String) result.get("text");

            this.ingredients.add(i);
        }

        // clear cached ingredient scoring
        this.ingredientScore = -1.0f;
    }

    /**
     * Rates recipe's ingredients according to user's preferences. Calculates an
     * aggregate score from every ingredients' rating.
     *
     * @return aggregate ingredient score normalized into range [0.0, 1.0]
     */
    public double getIngredientValue() {

        if (this.ingredientScore < 0) {
            double value = 0.0d;
            int total = 0;

            // Loop through all user-added ingredients
            for (Map<String, Object> row
                    : Select.from(ReseptiVelho.db, "ingredients")
                            .columns("name", "rating")
                            .getAll()) {

                String match = ((String) row.get("name")).toLowerCase();
                double score = (double) (int) row.get("rating");

                // Handle allergen scoring
                if (score == 0) {
                    // Offset allergens far back
                    score = -666;
                } else {
                    // Normalize rating
                    score = (score - Ingredient.NEUTRAL_RATING) / 5.0f;
                }

                // Match user-added ingredients as substrings with each
                // ingredient row of this recipe
                Iterator<RecipeIngredient> i = this.ingredients.iterator();
                while (i.hasNext()) {
                    RecipeIngredient ingredient = i.next();

                    // Sum everything that matches
                    if (ingredient.text.toLowerCase().contains(match)) {
                        value += score;
                    }
                    total++;
                }
            }

            this.ingredientScore = (total > 0)
                    ? (double) value / total
                    : 0.0d;
        }

        return this.ingredientScore;
    }

    /**
     * Tests if this recipe contains any allergens (ingredients that are rated
     * below 1).
     *
     * @return true if there are any allergens in the ingredient list.
     */
    public boolean hasAllergens() {
        return this.getIngredientValue() < -1.0;
    }

    /**
     * Saves this Recipe object as a row in the db. New row will be generated
     * and object's id updated for new objects. Otherwise id stays unmodified
     * and only the altered db columns are updated.
     *
     * @return true on success, false if failed
     */
    public boolean save() {
        ReseptiVelho.db.transaction.begin();

        // Don't save twice with the same external id
        if (Recipe.getByExternalId(this.external_id) == null) {

            if (this.id == 0) {
                this.id = Insert.into(ReseptiVelho.db, "recipes")
                        .columns("name", "url", "external_id", "image", "description")
                        .values("?,?,?,?,?", this.name, this.url, this.external_id, this.image, this.description)
                        .execute();
            } else {
                Update.table(ReseptiVelho.db, "recipes")
                        .set("name", this.name)
                        .set("url", this.url)
                        .set("external_id", this.external_id)
                        .set("image", this.image)
                        .set("description", this.description)
                        .set("comment", this.comment)
                        .set("rating", this.rating)
                        .where("id = ?", this.id)
                        .execute();

                throw new UnsupportedOperationException(
                        "SQL UPDATE clause not yet implemented");
            }

            if (this.id == 0) {
                throw new RuntimeException("Insert failed");
            }

            // Flush old ingredients
            Delete.from(ReseptiVelho.db, "recipe_ingredients")
                    .where("recipe_id = ?", this.id)
                    .execute();

            // Save ingredients
            for (RecipeIngredient i : ingredients) {
                i.recipe_id = this.id;
                i.save();
            }

            ReseptiVelho.db.transaction.commit();
            return true;
        } else {
            ReseptiVelho.db.transaction.rollBack();
            System.out.println("ei onnistunut.");
            return false;
        }
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        s.append("#").append(this.id).append(" ").append(this.name);

        Iterator<RecipeIngredient> i = this.ingredients.iterator();
        if (i.hasNext()) {
            String separator = " (";

            while (i.hasNext()) {
                s.append(separator).append(i.next().toString());
                separator = ", ";
            }

            s.append(')');
        }

        return s.toString();
    }

    @Override
    public int compareTo(Object o) {
        return Double.compare(((Recipe) o).getIngredientValue(),
                this.getIngredientValue());
    }
}
