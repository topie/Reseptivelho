/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.group2.reseptivelho;

import com.group2.reseptivelho.db.Select;
import java.util.Map;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.web.WebView;

/**
 * Root Node Class for the recipescene
 * @author Ryhmä 2. Ville Kautonen, Topi Eronen, Robert Karijärvi
 */
public class RecipePane extends BorderPane{
    private final ReseptiVelhoGUI gui;
    /**
     * Constructor
     * @param recipeindex, index of the recipe for the scene, used in searching db for specific recipe
     * @param guil Reference to the view as a whole.
     */
    public RecipePane(int recipeindex, ReseptiVelhoGUI guil) {
        super();
        this.gui = guil;
        this.setId("recipescene");
        BorderPane root = new BorderPane();
        root.setPadding(new Insets(10, 10, 20, 10));
        Select recipedata = Select.from(ReseptiVelho.db, "recipes")
                .columns("name", "url", "image")
                .where("id = ?", recipeindex);
        Map<String, Object> row = recipedata.getRow();
        Text title = new Text(row.get("name").toString());
        title.setId("titles");
        title.setTextAlignment(TextAlignment.CENTER);
        StackPane titlebg = new StackPane();
        titlebg.getChildren().add(title);
        titlebg.setId("image");
        Text recipe = new Text();
        final WebView wv = new WebView();
        wv.setPrefSize(300, 300);
        if (row.get("url") != null) {
            //recipe.setText(ReseptiVelho.messages.getString("descriptionscene")+"\n"+row.get("description").toString());
            wv.getEngine().load(row.get("url").toString());
        }
        recipe.setTextAlignment(TextAlignment.JUSTIFY);
        recipe.setId("largetextbox");
        Image pic;
        try {
            if (row.get("image") != null) {
                pic = new Image(row.get("image").toString());
            } else {
                pic = new Image("https://upload.wikimedia.org/wikipedia/commons/8/8c/JPEG_example_JPG_RIP_025.jpg");
            }
        } catch (IllegalArgumentException iae) {
            pic = new Image("https://upload.wikimedia.org/wikipedia/commons/8/8c/JPEG_example_JPG_RIP_025.jpg");
            System.out.println(ReseptiVelho.messages.getString("debugimage"));
        }
        Select recipeingdata = Select.from(ReseptiVelho.db, "recipe_ingredients")
                .columns("text")
                .where("recipe_id = ?", recipeindex);
        String ingsdesc = "";
        Text ingredients = new Text();
        while ((row = recipeingdata.getRow()) != null) {
            ingsdesc += "\n" + row.get("text");
            ingredients = new Text(ReseptiVelho.messages.getString("ingredientscene")+ingsdesc);
        }
        ingredients.setId("largetextbox");
        ImageView imgview = new ImageView();
        imgview.setImage(pic);
        imgview.setFitWidth(500);
        imgview.preserveRatioProperty().set(true);
        StackPane imageholder = new StackPane();
        imageholder.getChildren().add(imgview);
        imageholder.setId("image");
        BorderPane recipedetails = new BorderPane();
        StackPane ingredientsbg = new StackPane();
        ingredientsbg.getChildren().add(ingredients);
        ingredientsbg.setId("image");
        StackPane recipebg = new StackPane();
        recipebg.getChildren().add(wv);
        recipebg.setId("image");
        recipedetails.setTop(ingredientsbg);
        recipedetails.setCenter(recipebg);
        Button btn = new Button();
        btn.setId("back");
        btn.setText(ReseptiVelho.messages.getString("backbtn"));
        btn.setPadding(new Insets(20, 20, 20, 20));
        btn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                System.out.println(ReseptiVelho.messages.getString("debugscreenchange"));
                wv.getEngine().load("");
                gui.stage.setScene(gui.browserscene);
            }
        });
        root.setTop(titlebg);
        root.setLeft(recipedetails);
        root.setRight(imageholder);
        root.setBottom(btn);
        this.setCenter(root);

    }

}
